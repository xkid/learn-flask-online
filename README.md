# Learn Flask Online #

Host : [http://learn-flask-online.appspot.com/](http://learn-flask-online.appspot.com/)

**The idea is**  
1. Using [c9.io](http://c9.io) Coding  
2. Using [Bitbucket.org](http://Bitbucket.org) as Code Version Control  
3. Deploy to Google App Engine [console.cloud.google.com](http://console.cloud.google.com)  

__How to deploy in appengine__  
1. open google cloud shell  
2. git clone https://xkid@bitbucket.org/xkid/learn-flask-online.git  
3. appcfg.py -A learn-flask-online -V v10a update .  