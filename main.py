"""`main` is the top level module for your Flask application."""

# Import the Flask Framework
from flask import Flask
from flask import render_template
from pymongo import MongoClient
import requests
import json

app = Flask(__name__)
# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.

@app.route('/')
@app.route('/<name>')
def hello(name=None):
    """Return a friendly HTTP greeting."""
    return render_template('index.html', name=name)

@app.route('/insert/<msg>')
def ins_msg(msg=None):
    '''
    mongolab_uri = "mongodb://xxxxxx:xxxxxx@dsxxxxxx.mlab.com:00000/gaexxxxx"
    client = MongoClient(mongolab_uri)
    db = client["gaexxxxx"]
    result = db.ppl.insert({"msg":msg})
    '''
    url = "https://api.mlab.com/api/1/databases/gae-mongo/collections/ppl?apiKey=MyAPIKey"
    data = {'msg':msg}
    headers = {'Content-Type': 'application/json'}
    r = requests.post(url, headers=headers,data=json.dumps(data))
    return render_template('index.html',res=r.text)

@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, Nothing at this URL.', 404


@app.errorhandler(500)
def application_error(e):
    """Return a custom 500 error."""
    return 'Sorry, unexpected error: {}'.format(e), 500

if __name__ == '__main__':
  app.run(host='0.0.0.0',port='8080')